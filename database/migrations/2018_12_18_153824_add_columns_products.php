<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsProducts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('products', function (Blueprint $table) {
            $table->unsignedInteger('manual_id')->nullable()->after('product_number');
            $table->integer('page')->nullable()->after('manual_id');
            $table->string('image')->after('pdf')->nullable()->after('page');
            $table->string('thump')->after('image')->nullable()->after('image');

            $table->foreign('manual_id')->references('id')->on('manuals')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('products', function (Blueprint $table) {
            $table->dropColumn('manual_id');
            $table->dropColumn('page');
            $table->dropColumn('image');
            $table->dropColumn('thump');
        });
    }
}
