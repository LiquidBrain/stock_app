<?php
/**
 * Created by PhpStorm.
 * User: 123
 * Date: 15.11.2018
 * Time: 17:57
 */

namespace App;


use Illuminate\Database\Eloquent\Model;

class Supplier extends Model
{
    protected $fillable = [
        'name'
    ];

}