<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterProductServiceTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('product_service', function ($table) {
            $table->dropForeign(['service_id']);
            $table->dropForeign(['product_id']);
            $table->foreign('service_id')->references('id')->on('services')->onDelete('cascade');;
            $table->foreign('product_id')->references('id')->on('products')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('product_service', function ($table) {
            $table->dropForeign(['service_id']);
            $table->dropForeign(['product_id']);
            $table->foreign('service_id')->references('id')->on('services');
            $table->foreign('product_id')->references('id')->on('products');
        });
    }
}
