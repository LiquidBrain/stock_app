@extends('layouts.base')

@section('container')
    <div class="content p-4">
        @yield('content')
    </div>
@endsection