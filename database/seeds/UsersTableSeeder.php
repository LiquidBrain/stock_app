<?php

use App\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'Admin',
            'email' => 'admin@gmail.com',
            'password' => bcrypt('test123'),
            'role' => 'admin'
        ]);

        DB::table('users')->insert([
            'name' => 'Operator',
            'email' => 'operator@gmail.com',
            'password' => bcrypt('test123'),
            'role' => 'operator'
        ]);
    }
}
