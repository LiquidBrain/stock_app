<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <!-- For IOS qr code scanner-->
    <meta name="apple-mobile-web-app-capable" content="yes">

    <title>Putzmeister Nederland</title>

    <!-- Scripts -->
    <script type="text/javascript" src="https://webrtc.github.io/adapter/adapter-latest.js"></script>
    <script src="{{ asset('js/app.js') }}" defer></script>

<!-- Fonts -->
    <link rel="dns-prefetch" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">

    <!-- Styles -->
    <link href="{{ asset('css/fontawesome.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/style.css') }}" rel="stylesheet">
    <link href="{{ asset('css/custom.css') }}" rel="stylesheet">

    <script>
        window.user = {};
        @if(Auth::check())
            window.user = {!! json_encode(Auth::user()) !!};
        @endif
    </script>
</head>
<body>
<div id="app">
    <nav class="bg-dark navbar-main navbar navbar-expand-md">
        <div class="container-fluid">
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
                    aria-controls="navbarSupportedContent" aria-expanded="false">
                <span class="fa fa-bars"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <!-- Left Side Of Navbar -->
                <ul class="navbar-nav mr-auto">
                    <li class="nav-item active">
                        <router-link class="nav-link" to="/">
                            <span class="fa fa-home"></span>
                            <span>Dashboard</span>
                        </router-link>
                    </li>
                    @if (Auth::check() && Auth::user()->role === 'admin')
                        <li class="nav-item active">
                            <router-link class="nav-link" to="/users">
                                <span class="fa fa-users"></span>
                                <span>Gebruikers</span>
                            </router-link>
                        </li>
                        <li class="nav-item active">
                            <router-link class="nav-link" to="/categories">
                                <span class="fa fa-clipboard-list"></span>
                                <span>Categories</span>
                            </router-link>
                        </li>
                        <li class="nav-item active">
                            <router-link class="nav-link" to="/suppliers">
                                <span class="fa fa-truck"></span>
                                <span>Leveranciers</span>
                            </router-link>
                        </li>
                        <li class="nav-item active">
                            <router-link class="nav-link" to="/vehicles">
                                <span class="fa fa-car"></span>
                                <span>Voertuigen</span>
                            </router-link>
                        </li>
                        <li class="nav-item active">
                            <router-link class="nav-link" to="/products">
                                <span class="fa fa-box-open"></span>
                                <span>Producten</span>
                            </router-link>
                        </li>
                        <li class="nav-item active">
                            <router-link class="nav-link" to="/manuals">
                                <span class="far fa-file-pdf"></span>
                                <span>Handleidingen</span>
                            </router-link>
                        </li>
                        <li class="nav-item active">
                            <router-link class="nav-link" to="/weekly/message/list">
                                <span class="fa fa-comment-alt"></span>
                                <span>Wekelijkse bericht</span>
                            </router-link>
                        </li>
                        <li class="nav-item active">
                            <router-link class="nav-link" to="/products/requests">
                                <span class="fa fa-comment-alt"></span>
                                <span>Request products</span>
                            </router-link>
                        </li>
                    @endif
                </ul>

                <!-- Right Side Of Navbar -->
                <ul class="navbar-nav ml-auto">
                    <!-- Authentication Links -->
                    @guest
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                        </li>
                    @else
                        <li class="nav-item dropdown">
                            <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button"
                               data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                {{ Auth::user()->name }} <span class="caret"></span>
                            </a>

                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                <a class="dropdown-item" href="/settings">Instellingen</a>

                                <a class="dropdown-item" href="{{ route('logout') }}"
                                   onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                    {{ __('Logout') }}
                                </a>

                                <form id="logout-form" action="{{ route('logout') }}" method="POST"
                                      style="display: none;">
                                    @csrf
                                </form>
                            </div>
                        </li>
                    @endguest
                </ul>
            </div>
        </div>
    </nav>

    <main>
        <div class="flex-xl-nowrap">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12 py-3 bd-content">
                        @yield('container')
                    </div>
                </div>
            </div>
        </div>
    </main>
</div>
</body>
</html>
