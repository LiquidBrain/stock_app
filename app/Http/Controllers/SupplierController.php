<?php

namespace App\Http\Controllers;


use App\Product;
use App\Supplier;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class SupplierController extends Controller
{
    public function list()
    {
        $suppliers = Supplier::all(['id', 'name']);

        return $suppliers;
    }

    public function getSupplier($supplierId)
    {
        $supplier = Supplier::findOrFail($supplierId);

        return $supplier;
    }

    public function getAll()
    {
        $vehicles = Supplier::paginate(20);

        return $vehicles;
    }

    public function save(Request $request, $supplierId = null)
    {

        $data = $this->validate($request, [
            'name' => 'required',
        ]);

        if ($supplierId) {
            $supplier = Supplier::findOrFail($supplierId);
            $supplier->update($data);
        } else {
            $supplier = Supplier::create($data);
        }

        return $supplier;
    }

    public function delete(Request $request, $supplierId)
    {
        $product = Product::where('supplier_id', $supplierId)->first();
        if ($product) {
            return response([
                'success' => false,
                'message' => "You can't delete this supplier because it used in Product with id: $product->id"
            ], Response::HTTP_BAD_REQUEST);
        }

        $supplier = Supplier::findOrFail($supplierId);
        $supplier->delete();

        return ['success' => true, 'message' => 'Supplier was successfully deleted!'];
    }
}