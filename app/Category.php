<?php
/**
 * Created by PhpStorm.
 * User: 123
 * Date: 15.11.2018
 * Time: 17:56
 */

namespace App;


use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $fillable = [
        'name'
    ];
}