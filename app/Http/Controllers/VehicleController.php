<?php

namespace App\Http\Controllers;


use App\CheckList;
use App\Service;
use App\User;
use App\Vehicle;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Validation\Rule;

class VehicleController extends Controller
{
    public function getAll(Request $request)
    {
        /** @var User $user */
        $user = Auth::user();

        if ($user->role === 'operator') {

            $vehicles = [];

            $pomp = $user->pomp;

            if (sizeof($pomp) > 0) {
                $vehicles[] = $pomp[0]['id'];
            } else {
                $pomps = Vehicle::where('pump', 1)->get();

                foreach ($pomps as $pomp) {
                    $vehicles[] = $pomp['id'];
                }
            }

            $bus = $user->bus;

            if (sizeof($bus) > 0) {
                $vehicles[] = $bus[0]['id'];
            } else {
                $buses = Vehicle::where('pump', 0)->get();

                foreach ($buses as $bus) {
                    $vehicles[] = $bus['id'];
                }
            }

            $vehicles = Vehicle::whereIn('id', $vehicles)->paginate(20);

            return $vehicles;
        }

        return Vehicle::paginate(20);
    }

    public function list()
    {
        $vehicles = Vehicle::get();

        return $vehicles;
    }

    public function getPumpVehicles()
    {
        $vehicles = Vehicle::where('pump', true)->with('operators')->get();

        return $vehicles;
    }

    public function getVanVehicles(Request $request)
    {
        $vehicles = Vehicle::where('pump', false)->with('operators')->get();

        return $vehicles;
    }

    public function getVehicle($vehicleId)
    {
        $vehicle = Vehicle::where('id', $vehicleId)->first();

        return $vehicle;
    }

    public function save(Request $request, $vehicleId = null)
    {
        $rule = Rule::unique('vehicles');

        if ($vehicleId) {
            $rule->ignore($vehicleId);
        }

        $data = $this->validate($request, [
            'brand' => 'required',
            'type' => 'required',
            'sing' => 'required',
            'manufacture_date' => 'required',
            'chassis_number' => 'required',
            'nick_name' => 'required',
            'pump' => 'required',
            'datum' => 'sometimes',
            'ownership' => 'sometimes|file|mimes:pdf',
            'insureance' => 'sometimes|file|mimes:pdf',
            'inspection_report' => 'sometimes|file|mimes:pdf',
        ]);
//        dump($data);
        $data['pump'] = $data['pump'] == 'true' ? 1 : 0;

        $ownership = $data['ownership'] ?? null;
        $insureance = $data['insureance'] ?? null;
        $inspection_report = $data['inspection_report'] ?? null;

        unset($data['ownership']);
        unset($data['insureance']);
        unset($data['inspection_report']);

        if ($vehicleId) {
            $vehicle = Vehicle::findOrFail($vehicleId);
            $vehicle->update($data);
        } else {
            $vehicle = Vehicle::create($data);
        }

        //Save files
        $uploadsPath = 'uploads/vehicles/' . $vehicle->id;
        /**
         * @var $ownership UploadedFile
         * @var $insureance UploadedFile
         * @var $inspection_report UploadedFile
         */
        if ($ownership) {
            $fileName = pathinfo($ownership->getClientOriginalName(), PATHINFO_FILENAME);
            $fileName = str_slug($fileName) . '.' . $ownership->getClientOriginalExtension();
            $ownershipPath = $ownership->storeAs($uploadsPath, $fileName, ['disk' => 'public']);
            $vehicle->ownership = $ownershipPath;
        }

        if ($insureance) {
            $fileName = pathinfo($insureance->getClientOriginalName(), PATHINFO_FILENAME);
            $fileName = str_slug($fileName) . '.' . $insureance->getClientOriginalExtension();
            $insureancePath = $insureance->storeAs($uploadsPath, $fileName, ['disk' => 'public']);
            $vehicle->insureance = $insureancePath;
        }

        if ($inspection_report) {
            $fileName = pathinfo($inspection_report->getClientOriginalName(), PATHINFO_FILENAME);
            $fileName = str_slug($fileName) . '.' . $inspection_report->getClientOriginalExtension();
            $inspection_reportPath = $inspection_report->storeAs($uploadsPath, $fileName, ['disk' => 'public']);
            $vehicle->inspection_report = $inspection_reportPath;
        }

        $vehicle->save();

        return $vehicle;
    }

    public function delete(Request $request, $vehicleId)
    {
        $checklist = CheckList::where('vehicle_van', $vehicleId)
            ->orWhere('vehicle_pump', $vehicleId)
            ->first();

        if ($checklist) {
            return response([
                'success' => false,
                'message' => "You can't delete this vehicle because it used in checklist with id: $checklist->id"
            ], Response::HTTP_BAD_REQUEST);
        }

        $service = Service::where('vehicle_id', $vehicleId)->first();

        if ($service) {
            return response([
                'success' => false,
                'message' => "You can't delete this vehicle because it used in Onderhoudsrapporten with id: $service->id"
            ], Response::HTTP_BAD_REQUEST);
        }

        $vehicle = Vehicle::findOrFail($vehicleId);

        $vehicle->delete();

        return ['success' => true, 'message' => 'Vehicle was successfully deleted'];
    }



    public function saveRelation(Request $request){
        $data = $request->get('data');

        foreach ($data as $value) {

            /** @var User $operator * */
            $operator = User::findOrFail($value['id']);

            $ids = [];

            if ($value['busId']) {
                $ids[] = $value['busId'];
            }

            if ($value['pompId']) {
                $ids[] = $value['pompId'];
            }

            $operator->vehicles()->sync($ids);
        }

        return $operator;
    }
}