<?php

namespace App\Http\Controllers;


use App\CheckList;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ChecklistController extends Controller
{
    public function createPage($checklistId = null)
    {
        return view('checklist.edit', ['checklistId' => $checklistId]);
    }

    public function getAll(Request $request)
    {
        $vehicleId = $request->query->get('vehicle_id');
        $userId = $request->query->get('user_id');

        $query = CheckList::with(['vehicle', 'operator', 'pomp']);

        if ($vehicleId) {
            $query->where('vehicle_pump', $vehicleId);
        }

        if ($userId) {
            $query->where('user_id', $userId);
        }

        $checklist = $query->paginate(20);

        return $checklist;
    }

    public function get($checklistId)
    {
        $checklist = CheckList::with('vehicle')->where('id', $checklistId)->first();

        return $checklist;
    }

    public function save(Request $request, $checklistId = null)
    {
        $data = $this->validate($request, [
            'vehicle_pump' => 'required',
            'mileage' => 'required',
            'motor_oil' => 'required',
            'hydraulic_oil' => 'required',
            'cooling_water' => 'required',
            'wear_parts' => 'required',
            'drawbar' => 'required',
            'lubricating' => 'required',
            'vehicle_van' => 'required',
            'clamps' => 'required',
            'laser' => 'required',
            'concrete_placer' => 'required',
            'dobber' => 'required',
            'note' => 'sometimes',
        ], [
            'vehicle_pump.required' => 'Voer een geldige waarde in voor: Voertuig',
            'mileage.required' => 'Voer een geldige waarde in voor: Urenstand',
            'motor_oil.required' => 'Voer een geldige waarde in voor: Motorolie peil',
            'hydraulic_oil.required' => 'Voer een geldige waarde in voor: Hydrauliekolie peil',
            'cooling_water.required' => 'Voer een geldige waarde in voor: Spoelkast ververst',
            'wear_parts.required' => 'Voer een geldige waarde in voor: Opening slijtdelen in mm',
            'drawbar.required' => 'Voer een geldige waarde in voor: Dissel in orde (geen speling)',
            'lubricating.required' => 'Voer een geldige waarde in voor: Op 4 punten in de bak vet',
            'vehicle_van.required' => 'Voer een geldige waarde in voor: Bus',
            'clamps.required' => 'Voer een geldige waarde in voor: Aantal klemmen',
            'laser.required' => 'Voer een geldige waarde in voor: Laser aanwezig',
            'concrete_placer.required' => 'Voer een geldige waarde in voor: Betonhark aanwezig',
            'dobber.required' => 'Voer een geldige waarde in voor: Dobbers aanwezig',
        ]);

        $data['user_id'] = Auth::user()->id;

        if ($checklistId) {
            $checklist = CheckList::findOrFail($checklistId);
            $checklist->update($data);
        } else {
            $checklist = CheckList::create($data);
        }

        return $checklist;
    }

    public function delete(Request $request, $checklistId)
    {
        $checklist = CheckList::findOrFail($checklistId);

        $checklist->delete();

        return ['success' => true, 'message' => 'Check list was successfully deleted'];
    }
}