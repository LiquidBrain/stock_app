<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\Hash;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'role'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function vehicles()
    {
        return $this->belongsToMany(Vehicle::class, 'operator_vehicle', 'operator_id', 'vehicle_id');
    }

    public function pomp()
    {
        return $this->vehicles()->where('vehicles.pump', '=', 1);
    }

    public function bus()
    {
        return $this->vehicles()->where('vehicles.pump', '=', 0);
    }
}
