<?php
/**
 * Created by PhpStorm.
 * User: 123
 * Date: 18.12.2018
 * Time: 11:26
 */

namespace App\Http\Controllers;


use App\WeeklyMessage;
use Illuminate\Http\Request;

class WeeklyMessageController extends Controller
{
    public function getAll(Request $request)
    {
        $messages = WeeklyMessage::all();

        return $messages;
    }

    public function get($messageId)
    {
        $message = WeeklyMessage::where('id', $messageId)->first();

        return $message;
    }

    public function getLast()
    {
        $message = WeeklyMessage::orderBy('id', 'desc')->first();

        return $message;
    }

    public function save(Request $request, $messageId = null)
    {

        $data = $this->validate($request, [
            'message' => 'required'
        ]);

        if ($messageId) {
            $message = WeeklyMessage::findOrFail($messageId);
            $message->update($data);
        } else {
            $message = WeeklyMessage::create($data);
        }

        return $message;
    }

    public function delete(Request $request, $messageId)
    {
        $message = WeeklyMessage::findOrFail($messageId);

        $message->delete();

        return ['success' => true, 'message' => 'Message was successfully deleted'];
    }
}