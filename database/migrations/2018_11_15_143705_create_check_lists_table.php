<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCheckListsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('check_lists', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('user_id');
            $table->integer('vehicle_pump');
            $table->integer('mileage');
            $table->boolean('motor_oil');
            $table->boolean('hydraulic_oil');
            $table->boolean('cooling_water');
            $table->integer('wear_parts');
            $table->boolean('drawbar');
            $table->boolean('lubricating');
            $table->unsignedInteger('vehicle_van');
            $table->integer('clamps');
            $table->boolean('laser');
            $table->boolean('concrete_placer');
            $table->boolean('dobber');
            $table->text('note')->nullable();
            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('vehicle_van')->references('id')->on('vehicles');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('check_lists');
    }
}
