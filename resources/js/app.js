require('./bootstrap');
require('core-js/fn/promise/finally');

window.Vue = require('vue');

Vue.component('loading', require('./components/Common/Loading.vue'));
Vue.component('file-input', require('./components/Common/FileInput.vue'));
Vue.component('original', require('./components/Common/Originalimg.vue'));
Vue.component('qr-scanner', require('./components/Common/QrScanner.vue'));
Vue.component('pagination', require('laravel-vue-pagination'));

import izitoast from 'izitoast';
import axios from 'axios';
import router from './src/router';
import VueSelect from 'vue-cool-select'

Vue.use(VueSelect, {
    theme: 'bootstrap'
});


axios.interceptors.response.use(null, function (error) {
    if (error.response.data) {
        const mess = Object.keys(error.response.data.errors)[0];
        console.log(mess);
        izitoast.show({
            color: 'red',
            title: 'Error!',
            message: error.response.data.errors[mess][0],
        });
    }

    return Promise.reject(error);
});

const app = new Vue({
    router: router,
    el: '#app'
});
