<?php

namespace App;


use Illuminate\Database\Eloquent\Model;

class OrderRequests extends model
{
    protected $fillable = [
        'user_id',
        'product_id',
        'amount',
        'description',
        'is_processed'
    ];
    public function product()
    {
        return $this->belongsTo(Product::class, 'product_id', 'id');
    }
    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }
}