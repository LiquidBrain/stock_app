<?php
/**
 * Created by PhpStorm.
 * User: 123
 * Date: 21.11.2018
 * Time: 13:57
 */

namespace App;

use App\Vehicle;
use App\User;
use Illuminate\Database\Eloquent\Model;

class CheckList extends Model
{
    protected $fillable = [
        'user_id',
        'date_now',
        'vehicle_pump',
        'mileage',
        'motor_oil',
        'hydraulic_oil',
        'cooling_water',
        'wear_parts',
        'drawbar',
        'lubricating',
        'vehicle_van',
        'clamps',
        'laser',
        'concrete_placer',
        'dobber',
        'note'
    ];
    public function operator()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }
    public function vehicle()
    {
        return $this->belongsTo(Vehicle::class, 'vehicle_pump', 'id');
    }

    public function pomp()
    {
        return $this->belongsTo(Vehicle::class, 'vehicle_van', 'id');
    }

}