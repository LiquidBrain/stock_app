<?php

namespace App\Http\Controllers;


use App\Category;
use App\Product;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    public function list()
    {
        $category = Category::all(['id', 'name']);

        return $category;
    }

    public function getCategory($categoryId)
    {
        $category = Category::where('id', $categoryId)->first();

        return $category;
    }

    public function getAll()
    {
        $category = Category::paginate(20);

        return $category;
    }

    public function save(Request $request, $categoryId = null)
    {

        $data = $this->validate($request, [
            'name' => 'required',
        ]);

        if ($categoryId) {
            $category = Category::findOrFail($categoryId);
            $category->update($data);
        } else {
            $category = Category::create($data);
        }

        return $category;
    }

    public function delete(Request $request, $categoryId)
    {
        $product = Product::where('category_id', $categoryId)->first();
        if($product){

            return response(['success' => false, 'message' => "You can't delete this category because it used in Product with id: $product->id"], 400);
        }
        $category = Category::findOrFail($categoryId);
        $category->delete();

        return ['success' => true, 'message' => 'Category was successfully deleted!'];
    }
}