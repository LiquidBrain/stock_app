<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOperatorVehicleTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('operator_vehicle', function (Blueprint $table) {
            $table->unsignedInteger('operator_id');
            $table->unsignedInteger('vehicle_id');
            $table->primary(['operator_id', 'vehicle_id']);

            $table->foreign('operator_id')->references('id')->on('users');
            $table->foreign('vehicle_id')->references('id')->on('vehicles');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('operator_vehicle', function (Blueprint $table) {
            $table->dropForeign('operator_vehicle_operator_id_foreign');
            $table->dropForeign('operator_vehicle_vehicle_id_foreign');
            $table->dropColumn('operator_id');
        });
        Schema::dropIfExists('operator_vehicle');
    }
}
