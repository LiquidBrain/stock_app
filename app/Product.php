<?php
/**
 * Created by PhpStorm.
 * User: 123
 * Date: 15.11.2018
 * Time: 17:56
 */

namespace App;


use App\Category;
use App\Supplier;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $fillable = [
        'title',
        'description',
        'category_id',
        'supplier_id',
        'stock_werkendam',
        'stock_tholen',
        'stock_zandijk',
        'price',
        'product_number',
        'manual_id',
        'page',
        'image',
        'thump'
    ];

    protected $with = ['manual'];

    public function category()
    {
        return $this->belongsTo(Category::class, 'category_id', 'id');
    }

    public function supplier()
    {
        return $this->belongsTo(Supplier::class, 'supplier_id', 'id');
    }

    public function manual()
    {
        return $this->belongsTo(Manual::class);
    }
}