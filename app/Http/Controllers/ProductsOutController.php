<?php

namespace App\Http\Controllers;


use App\ProductsOut;
use App\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ProductsOutController extends Controller
{
    public function getAll(Request $request)
    {
        $productId = $request->query->get('product_id');
        $operatorId = $request->query->get('operator_id');

        $query = ProductsOut::with(['operator', 'product', 'user']);
        if ($productId) {
            $query->where('product_id', $productId);
        }
        if ($operatorId) {
            $query->where('operator_id', $operatorId);
        }

        $category = $query->paginate(20);

        return $category;
    }

    public function save(Request $request, $productId = null)
    {
        $data = $this->validate($request, [
            'product_id' => 'required|exists:products,id',
            'operator_id' => 'required|exists:users,id',
            'description' => 'required',
            'stock' => 'required',
            'amount' => 'required',
        ]);

        $data['user_id'] = Auth::user()->id;

        $product = Product::findOrFail($data['product_id']);
        $field = 'stock_' . $data['stock'];
        $product->$field = $product->$field - $data['amount'];
        $product->save();

        $productOut = ProductsOut::create($data);

        return $productOut;
    }
}