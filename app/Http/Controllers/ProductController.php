<?php

namespace App\Http\Controllers;

use App\OrderRequests;
use App\Product;
use Egulias\EmailValidator\Warning\ObsoleteDTEXT;
use Illuminate\Http\Request;

// include composer autoload
//require 'vendor/autoload.php';

// import the Intervention Image Manager Class
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Auth;
use Intervention\Image\ImageManagerStatic as Image;

class ProductController extends Controller
{
    public function getAll()
    {
        $category = Product::with(['category', 'supplier'])->orderBy('id', 'desc')->paginate(20);

        return $category;
    }

    public function getRequests()
    {

        $category = OrderRequests::with(['product', 'user'])
            ->orderBy('id', 'DESC')
            ->paginate(20);

        return $category;
    }

    public function getProduct($productId = null)
    {

        $product = Product::where('id', $productId)->first();

        return $product;
    }

    public function getList()
    {
        $product = Product::get();

        return $product;
    }

    public function searchProduct(Request $request, $productNumber = null)
    {
        $isAlert = $request->query->get('alert');

        $productNumber = ltrim($productNumber, '0');

        $product = Product::with(['category', 'supplier'])
            ->where('product_number', 'LIKE', '%' . $productNumber . '%')
            ->get();

        if ($isAlert && count($product) === 0) {
            return response(['error' => true, 'message' => 'Producten ' . $productNumber . ' niet gevonden!'], 404);
        }

        return $product;
    }

    public function editPage($productId = null)
    {
        return view('products.edit', ['productId' => $productId]);
    }

    public function deleteImage(Request $request, $productId)
    {
        $product = Product::findOrFail($productId);
        $product->image = null;
        $product->thump = null;
        $product->save();

        return ['success' => true, 'message' => 'Image was successfully deleted!'];
    }

    public function save(Request $request, $productId = null)
    {

        $data = $this->validate($request, [
            'title' => 'required',
            'category_id' => 'required|exists:categories,id',
            'supplier_id' => 'required|exists:suppliers,id',
            'description' => 'required',
            'price' => 'required',
            'product_number' => 'required|unique:products,product_number,' . $productId,
            'stock_werkendam' => 'required',
            'stock_tholen' => 'required',
            'stock_zandijk' => 'required',
            'manual_id' => 'sometimes',
            'page' => 'sometimes',
            'image' => 'sometimes|file|mimes:jpeg,jpg,png'
        ]);

        if (empty($data['manual_id'])) {
            $data['page'] = null;
            $data['manual_id'] = null;
        }

        /**
         * @var UploadedFile $image
         */
        $image = $data['image'] ?? null;
        unset($data['image']);

        if ($productId) {
            $product = Product::findOrFail($productId);
            $product->update($data);
        } else {
            $product = Product::create($data);
        }

        $uploadsPath = 'uploads/products/images/' . $product->id;

        if ($image) {
            $imageName = str_slug($image->getClientOriginalName(), '_') . '.' . $image->getClientOriginalExtension();
            $imagePath = $image->storeAs($uploadsPath . '/originals', $imageName, ['disk' => 'public']);
            $thumpPath = $image->storeAs($uploadsPath . '/thump', $imageName, ['disk' => 'public']);

            $img = Image::make($thumpPath);
            $img->fit(200, 200);
            $img->save();

            $product->image = $imagePath;
            $product->thump = $thumpPath;
            $product->save();
        }

        return $product;
    }

    public function delete(Request $request, $supplierId)
    {
        $user = Product::findOrFail($supplierId);
        $user->delete();

        return ['success' => true, 'message' => 'Product was successfully deleted!'];
    }

    public function requestProcessed(Request $request, $requestId)
    {
        $data['is_processed'] = true;
        $request = OrderRequests::findOrFail($requestId);
        $request->update($data);

        return ['success' => true, 'message' => 'Request was successfully processed!'];
    }

    public function saveRequest(Request $request, $productId)
    {
        $user = Auth::user();

        $data = $this->validate($request, [
            'amount' => 'required',
            'description' => 'required',
        ]);

        $data['user_id'] = $user->id;
        $data['product_id'] = $productId;

        $product = OrderRequests::create($data);

        return $product;
    }
}