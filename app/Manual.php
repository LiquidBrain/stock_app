<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Manual extends Model
{
    protected $fillable = [
        'title',
        'path',
        'size'
    ];

    protected $appends = ['ext'];

    public function getExtAttribute($attributes)
    {
        return pathinfo($this->path, PATHINFO_EXTENSION);
    }
}