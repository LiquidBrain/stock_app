import Vue from 'vue';
import VueRouter from "vue-router";

Vue.use(VueRouter);

import Main from '../components/Main';

import UserList from '../components/Users/List';
import UserFrom from '../components/Users/Form';

import VehicleList from '../components/Vehicles/List';
import VehicleFrom from '../components/Vehicles/Form';

import OperatorVehicle from '../components/OperatorVehicle/OperatorVehicle';

import SupplierList from '../components/Suppliers/List';
import SupplierForm from '../components/Suppliers/Form';

import CategoryList from '../components/Category/List';
import CategoryForm from '../components/Category/Form';

import ProductList from '../components/Products/List';
import ProductForm from '../components/Products/Form';
import ProductOut from '../components/Products/Out';
import ProductRequest from '../components/Products/Request';
import RequestList from '../components/Products/RequestList';

import MaintenanceList from '../components/Maintenance/List';
import MaintenanceForm from '../components/Maintenance/Form';

import ChangePassword from '../components/Settings/ChangePassword';

import ReportsDelivery from '../components/Stock/ReportsDelivery';

import Stock from '../components/Stock/Stock';

import ManualList from '../components/Manual/List';
import ManualForm from '../components/Manual/Form';
import ManualButtons from '../components/Manual/Buttons';

import WeeklyMessageList from '../components/WeeklyMessage/List';
import WeeklyMessageForm from '../components/WeeklyMessage/Form';

import NotFound from '../components/NotFound';

//CheckUp
import CheckUpList from '../components/CheckUp/List.vue';
import CheckUpForm from '../components/CheckUp/Form.vue';

const router = new VueRouter({
    mode: 'history',
    routes: [
        //Main
        {
            path: '/',
            component: Main
        },

        //Users
        {
            path: '/users',
            component: UserList,
            meta: {
                roles: ['admin'],
                title: 'Gebruikers'
            }
        }, {
            path: '/users/create',
            component: UserFrom,
            meta: {
                roles: ['admin'],
                title: 'Nieuwe Gebruiker'
            }
        }, {
            path: '/users/:userId/edit',
            component: UserFrom, props: true,
            meta: {
                roles: ['admin'],
                title: 'Bewerk Gebruiker'
            }
        },

        //Vehicle
        {
            path: '/vehicles',
            component: VehicleList,
            meta: {
                roles: ['admin', 'operator', 'meganic'],
                title: 'Voertuigen'
            }
        }, {
            path: '/vehicles/create',
            component: VehicleFrom,
            meta: {
                roles: ['admin'],
                title: 'Voertuig toevoegen'
            }
        }, {
            path: '/vehicles/:vehicleId/edit',
            component: VehicleFrom, props: true,
            meta: {
                roles: ['admin'],
                title: 'Voertuig bewerk'
            }
        },

        //Supplier
        {
            path: '/suppliers',
            component: SupplierList,
            meta: {
                roles: ['admin'],
                title: 'Leveranciers'
            }
        }, {
            path: '/suppliers/create',
            component: SupplierForm,
            meta: {
                roles: ['admin'],
                title: 'Leverancier toevoegen'
            }
        }, {
            path: '/suppliers/:supplierId/edit',
            component: SupplierForm, props: true,
            meta: {
                roles: ['admin'],
                title: 'Leverancier bewerk'
            }
        },

        //Category
        {
            path: '/categories',
            component: CategoryList,
            meta: {
                roles: ['admin'],
                title: 'Categories'
            }
        }, {
            path: '/categories/create',
            component: CategoryForm,
            meta: {
                roles: ['admin'],
                title: 'Create category'
            }
        }, {
            path: '/categories/:categoryId/edit',
            component: CategoryForm, props: true,
            meta: {
                roles: ['admin'],
                title: 'Edit category'
            }
        },

        //Product
        {
            path: '/products',
            component: ProductList,
            meta: {
                roles: ['admin'],
                title: 'Producten'
            }
        }, {
            path: '/products/create',
            component: ProductForm,
            meta: {
                roles: ['admin'],
                title: 'Producten toevoegen'
            }
        }, {
            path: '/products/:productId/edit',
            component: ProductForm, props: true,
            meta: {
                roles: ['admin'],
                title: 'Producten bewerk'
            }
        }, {
            path: '/products/:productId/request',
            component: ProductRequest, props: true,
            meta: {
                roles: ['admin', 'meganic'],
                title: 'Producten aanvragen'
            }
        }, {
            path: '/products/:productId/out',
            component: ProductOut, props: true,
            meta: {
                roles: ['admin', 'meganic'],
                title: 'Product Out'
            }
        }, {
            path: '/products/requests',
            component: RequestList,
            meta: {
                roles: ['admin'],
                title: 'Producten'
            }
        },

        //Checklist
        {
            path: '/checklist',
            component: CheckUpList,
            meta: {
                roles: ['admin', 'meganic'],
                title: 'Checklijsten'
            }
        }, {
            path: '/checklist/create',
            component: CheckUpForm,
            meta: {
                roles: ['admin', 'operator', 'meganic'],
                title: 'Checklijsten toevoegen'
            }
        }, {
            path: '/checklist/:id/edit',
            component: CheckUpForm, props: true,
            meta: {
                roles: ['admin'],
                title: 'Checklijsten bewerk'
            }
        }, {
            path: '/vehicles/operator',
            component: OperatorVehicle, props: true,
            meta: {
                roles: ['admin', 'meganic'],
                title: 'Operator & Vehicle'
            }
        },

        //Common
        {
            path: '/stock',
            component: Stock,
            meta: {
                roles: ['admin', 'meganic'],
                title: 'Voorraad'
            }
        }, {
            path: '/reports/delivery',
            component: ReportsDelivery,
            meta: {
                roles: ['admin', 'meganic'],
                title: 'Goederenuitgifte'
            }
        }, {
            path: '/maintenance',
            component: MaintenanceList,
            meta: {
                roles: ['admin', 'meganic'],
                title: 'Onderhoudsrapport'
            }
        }, {
            path: '/maintenance/create',
            component: MaintenanceForm,
            meta: {
                roles: ['admin', 'meganic'],
                title: 'Nieuw Onderhoudsrapport'
            }
        },
        {
            path: '/maintenance/:id/edit',
            component: MaintenanceForm, props: true,
            meta: {
                roles: ['admin', 'meganic'],
                title: 'Nieuw Onderhoudsrapport'
            }
        },
        //Settings
        {
            path: '/settings/',
            component: ChangePassword,
            meta: {
                roles: ['admin', 'operator', 'meganic'],
                title: 'Paswoord wijzigen'
            }
        },
        //Manual
        {
            path: '/manuals',
            component: ManualList,
            meta: {
                roles: ['admin'],
                title: 'Handleidingen'
            }
        },
        {
            path: '/manuals/create',
            component: ManualForm,
            meta: {
                roles: ['admin'],
                title: 'Handleidingen'
            }
        },
        {
            path: '/manual',
            component: ManualButtons,
            meta: {
                title: 'Handleidingen'
            }
        },
        {
            path: '/weekly/message/list',
            component: WeeklyMessageList,
            meta: {
                roles: ['admin', 'meganic'],
                title: 'Wekelijks bericht'
            }
        },
        {
            path: '/weekly/message/create',
            component: WeeklyMessageForm,
            meta: {
                roles: ['admin'],
                title: 'Wekelijkse bericht'
            }
        },
        {
            path: '/weekly/message/:messageId/edit',
            component: WeeklyMessageForm, props: true,
            meta: {
                roles: ['admin'],
                title: 'Wekelijkse bericht'
            }
        },
        {
            path: '*',
            component: NotFound
        }
    ]
});

router.beforeEach((to, from, next) => {
    const requireRoles = to.meta.roles || [];
    const title = to.meta.title || null;
    const user = window.user;

    if (!requireRoles.length || requireRoles.indexOf(user.role) !== -1) {
        document.title = 'Putzmeister Nederland' + (title ? ' | ' + title : '');
        next();
    } else {
        next('/');
    }
});

export default router;