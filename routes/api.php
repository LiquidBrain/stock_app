<?php

use Illuminate\Support\Facades\Route;

Route::middleware('auth')->group(function () {
    //Users
    Route::get('/users', 'UserController@getAll');
    Route::get('/users/operators/all', 'UserController@getOperatorAll');
    Route::get('/user', 'UserController@getUser');
    Route::get('/users/current', 'UserController@current');
    Route::get('/users/list', 'UserController@getUsersList');
    Route::get('/users/operators', 'UserController@getOperators');
    Route::get('/users/{user}', 'UserController@get');
    Route::post('/users/create', 'UserController@save');
    Route::post('/users/{user}/save', 'UserController@save');
    Route::post('/users/change-password', 'UserController@changePassword');
    Route::post('/users/{user}/delete', 'UserController@delete');

    //Supplier
    Route::get('/suppliers', 'SupplierController@getAll');
    Route::get('/suppliers/list', 'SupplierController@list');
    Route::get('/supplier/{supplier}', 'SupplierController@getSupplier');
    Route::post('/supplier/create', 'SupplierController@save');
    Route::post('/supplier/{supplier}/save', 'SupplierController@save');
    Route::post('/supplier/{supplier}/delete', 'SupplierController@delete');

    //Category
    Route::get('/categories', 'CategoryController@getAll');
    Route::get('/categories/list', 'CategoryController@list');
    Route::get('/category/{category}', 'CategoryController@getCategory');
    Route::post('/category/create', 'CategoryController@save');
    Route::post('/category/{category}/save', 'CategoryController@save');
    Route::post('/category/{category}/delete', 'CategoryController@delete');

    //Vehicle
    Route::get('/vehicles', 'VehicleController@getAll');
    Route::get('/vehicles/pump', 'VehicleController@getPumpVehicles');
    Route::get('/vehicles/van', 'VehicleController@getVanVehicles');
    Route::get('/vehicles/list', 'VehicleController@list');
    Route::get('/vehicles/{vehicle}', 'VehicleController@getVehicle');
    Route::post('/vehicles/create', 'VehicleController@save');
    Route::post('/vehicles/{vehicle}/save', 'VehicleController@save');
    Route::post('/vehicles/{vehicle}/delete', 'VehicleController@delete');
    Route::get('/vehicles/relation', 'VehicleController@getOperator');
    Route::post('/vehicles/save/{relation}', 'VehicleController@saveRelation');

    //Product
    Route::get('/products', 'ProductController@getAll');
    Route::get('/products/requests', 'ProductController@getRequests');
    Route::get('/products/list', 'ProductController@getList');
    Route::get('/products/{product}', 'ProductController@getProduct');
    Route::get('/products/search/{productNumber}', 'ProductController@searchProduct');
    Route::post('/products/create', 'ProductController@save');
    Route::post('/products/{product}/save', 'ProductController@save');
    Route::post('/products/{product}/image/delete', 'ProductController@deleteImage');
    Route::post('/products/{product}/delete', 'ProductController@delete');
    Route::post('/products/{product}/request', 'ProductController@saveRequest');
    Route::post('/products/{product}/processed', 'ProductController@requestProcessed');


    //ProductOut
    Route::post('/productout/create', 'ProductsOutController@save');
    Route::get('/productout', 'ProductsOutController@getAll');

    //CheckList
    Route::get('/checklist', 'ChecklistController@getAll');
    Route::post('/checklist/create', 'ChecklistController@save');
    Route::get('/checklist/{checklist}', 'ChecklistController@get');
    Route::post('/checklist/{checklist}/save', 'ChecklistController@save');
    Route::post('/checklist/{checklist}/delete', 'ChecklistController@delete');

    //Maintenance
    Route::get('/maintenance', 'MaintenanceController@getAll');
    Route::post('/maintenance/create', 'MaintenanceController@save');
    Route::post('/maintenance/{maintenance}/save', 'MaintenanceController@save');
    Route::get('/maintenance/{maintenance}', 'MaintenanceController@get');

    //Manuals
    Route::get('/manuals', 'ManualController@getAll');
    Route::get('/manuals/list', 'ManualController@list');
    Route::post('/manuals/create', 'ManualController@save');
    Route::post('/manuals/{manual}/delete', 'ManualController@delete');

    //WeeklyMessage
    Route::get('/messages', 'WeeklyMessageController@getAll');
    Route::get('/message', 'WeeklyMessageController@getLast');
    Route::post('/message/create', 'WeeklyMessageController@save');
    Route::get('/message/{message}', 'WeeklyMessageController@get');
    Route::post('/message/{message}/save', 'WeeklyMessageController@save');
    Route::post('/message/{message}/delete', 'WeeklyMessageController@delete');
});
