<?php

namespace App\Http\Controllers;

use App\Manual;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Http\UploadedFile;

class ManualController extends Controller
{
    public function getAll()
    {
        $manual = Manual::orderBy('id', 'desc')->paginate(20);

        return $manual;
    }

    public function list()
    {
        return Manual::all();
    }

    public function save(Request $request)
    {
        $data = $this->validate($request, [
            'title' => 'required',
            'file' => 'required|file|mimes:pdf,doc,docx,jpeg,jpg,png'
        ]);

        /**
         * @var UploadedFile $file
         */
        $file = $data['file'];
        $filename = str_slug($data['title']) . '.' . $file->getClientOriginalExtension();
        $path = $file->storeAs('uploads/manuals', $filename, ['disk' => 'public']);

        $manual = Manual::create([
            'title' => $data['title'],
            'path' => $path,
            'size' => $file->getSize(),
        ]);

        return $manual;
    }

    public function delete(Request $request, $id)
    {
        /**
         * @var Model $manual
         */
        $manual = Manual::findOrFail($id);

        if ($manual) {
            $path = public_path() . DIRECTORY_SEPARATOR . $manual->path;

            if (is_file($path)) {
                unlink($path);
            }

            $manual->delete();
        }

        return ['success' => true, 'message' => 'Handleiding succesvol verwijderd!'];
    }
}