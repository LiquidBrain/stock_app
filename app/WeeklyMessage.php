<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class WeeklyMessage extends Model
{
    protected $fillable = [
        'message'
    ];
}