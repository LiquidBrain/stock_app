<?php
/**
 * Created by PhpStorm.
 * User: 123
 * Date: 15.11.2018
 * Time: 17:55
 */

namespace App;


use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Vehicle extends Model
{
    protected $fillable = [
        'brand',
        'type',
        'sing',
        'manufacture_date',
        'chassis_number',
        'pump',
        'nick_name',
        'datum',
        'ownership',
        'insureance',
        'inspection_report'

    ];

    public function setManufactureDateAttribute($date)
    {
        $this->attributes['manufacture_date'] = Carbon::parse($date);
    }

    public function getManufactureDateAttribute()
    {
        return Carbon::parse($this->attributes['manufacture_date'])->format('Y-m-d');
    }

    public function operators()
    {
        return $this->belongsToMany(User::class, 'operator_vehicle', 'vehicle_id','operator_id');
    }
}