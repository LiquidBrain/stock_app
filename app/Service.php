<?php
/**
 * Created by PhpStorm.
 * User: 123
 * Date: 15.11.2018
 * Time: 17:37
 */

namespace App;


use App\Product;
use Illuminate\Database\Eloquent\Model;

class Service extends Model
{
    protected $fillable = [
        'meganic_id',
        'vehicle_id',
        'mileage',
        'description',
    ];

    public function vehicle()
    {
        return $this->belongsTo(Vehicle::class);
    }

    public function meganic()
    {
        return $this->belongsTo(User::class, 'meganic_id', 'id');
    }

    public function products()
    {
        return $this->belongsToMany(Product::class)->withPivot('amount');;
    }
}