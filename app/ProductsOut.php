<?php
/**
 * Created by PhpStorm.
 * User: 123
 * Date: 22.11.2018
 * Time: 16:13
 */

namespace App;

//use App\Http\Controllers\ProductController;
use App\Product;
use App\User;
use Illuminate\Database\Eloquent\Model;

class ProductsOut extends Model
{
    protected $fillable = [
        'product_id',
        'operator_id',
        'user_id',
        'description',
        'amount',
        'stock'
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function operator()
    {
        return $this->belongsTo(User::class, 'operator_id', 'id');
    }

    public function product()
    {
        return $this->belongsTo(Product::class, 'product_id', 'id');
    }
}