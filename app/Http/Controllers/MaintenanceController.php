<?php

namespace App\Http\Controllers;

use App\Product;
use App\Service;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class MaintenanceController extends Controller
{
    public function getAll(Request $request)
    {
        $query = Service::with(['products', 'meganic', 'vehicle']);
        $vehicleId = $request->request->getInt('vehicle_id');

        if ($vehicleId) {
            $query->where('vehicle_id', $vehicleId);
        }

        $services = $query->orderBy('id', 'desc')->paginate(20);

        return $services;
    }

    public function get($id)
    {
        $message = Service::with(['products', 'products.category'])->where('id', $id)->first();

        return $message;
    }

    public function save(Request $request, $serviceId = null)
    {
        $isSubtract = $request->request->get('is_subtract');
        $data = $this->validate($request, [
            'vehicle_id' => 'required|exists:vehicles,id',
            'mileage' => 'required',
            'description' => 'required',
            'products' => 'sometimes'
        ]);

        /*$service = Service::create(array_merge($data, [
            'meganic_id' => Auth::user()->id
        ]));*/

        if ($serviceId) {
            $service = Service::findOrFail($serviceId);
            $service->update($data);
        } else {
            $service = Service::create(array_merge($data, [
            'meganic_id' => Auth::user()->id
        ]));
        }

        if (!empty($data['products'])) {
            $products = array_map(function ($value) {
                return ['amount' => $value];
            }, array_count_values($data['products']));

            $service->products()->sync($products);

            if ($isSubtract) {
                foreach ($products as $productId => $value) {
                    $product = Product::find($productId);

                    if ($product) {
                        $product->stock_werkendam = $product->stock_werkendam - $value['amount'];
                        $product->save();
                    }
                }
            }
        }

        return $service;
    }
}