<?php

namespace App\Http\Controllers;


use App\CheckList;
use App\Service;
use App\User;
use App\Vehicle;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;


class UserController extends Controller
{
    public function get($userId)
    {
        $user = User::findOrFail($userId);

        return $user;
    }

    public function current()
    {
        return Auth::user();
    }

    public function getUsersList()
    {
        $users = User::all();

        return $users;
    }

    public function getUser()
    {
        $users = User::where('id', Auth::user()->id)->first();

        return $users;
    }

    public function getAll()
    {
        $users = User::with(['bus', 'pomp'])->paginate(20);

        return $users;
    }

    public function getOperatorAll()
    {
        $users = User::with(['bus', 'pomp'])->where('role', 'operator')->paginate(20);

        return $users;
    }

    public function getAllUsers()
    {
        $rows = intval(request()->get('rows', 50));
        $users = User::orderBy('id', 'desc')->paginate($rows);

        return $users;
    }

    public function getUsers($userId)
    {

        $user = User::where('id', $userId)->first();

        return $user;
    }

    public function getOperators()
    {
        $users = User::where('role', 'operator')->get();

        return $users;
    }

    public function changePassword(Request $request, $userId = null)
    {
        $user = Auth::user();

        $data = $this->validate($request, [
            'old_password' => ['required', function ($attribute, $value, $fail) use ($user) {
                if (!Hash::check($value, $user->password)) {
                    return $fail('The current password is incorrect.');
                }
            }],
            'password' => 'required|min:6',
            'verify_password' => 'required|same:password',
        ]);

        $user->password = Hash::make($data['password']);
        $user->save();

        return ['success' => true, 'message' => 'Password changed'];
    }

    public function save(Request $request, $userId = null)
    {
        $rules = [
            'name' => 'required',
            'role' => 'required',
            'email' => 'required|email|unique:users,email,' . $userId,
        ];

        if (!$userId || $request->request->get('password')) {
            $rules['password'] = 'required|min:6|max:16|confirmed';
        }

        $data = $this->validate($request, $rules);

        if (!empty($data['password'])) {
            $data['password'] = Hash::make($data['password']);
        }

        if ($userId) {
            $user = User::findOrFail($userId);
            $user->update($data);
        } else {
            $user = User::create($data);
        }

        return $user;
    }

    public function delete(Request $request, $userId)
    {
        if ($vehicle = Vehicle::where('operator_id', $userId)->first()) {
            return response([
                'success' => false,
                'message' => "You can't delete this user because it used in Vehicle with id: $vehicle->id"
            ], Response::HTTP_BAD_REQUEST);
        }

        if ($service = Service::where('meganic_id', $userId)->first()) {
            return response([
                'success' => false,
                'message' => "You can't delete this user because it used in Onderhoudsrapporten with id: $service->id"
            ], Response::HTTP_BAD_REQUEST);
        }

        if ($checklist = CheckList::where('user_id', $userId)->first()) {
            return response([
                'success' => false,
                'message' => "You can't delete this user because it used in CheckList with id: $checklist->id"
            ], Response::HTTP_BAD_REQUEST);
        }

        if ($userId == 1) {
            throw new BadRequestHttpException('Not allowed to delete the main user');
        }

        $user = User::findOrFail($userId);
        $user->delete();

        return ['success' => true, 'message' => 'User was successfully deleted!'];
    }
}